//
//  ESSimulator.h
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/26/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESFloor.h"

@protocol ESSimulatorProtocol;

/**
 This class contains all the logic to simulate a complete elevator.
 Other models - ESElevator and ESFloor are dumb in the sense that they are just
 aware of their own state.
 */
@interface ESSimulator : NSObject
- (instancetype)initWithFloorsCount:(NSUInteger)floorCount
            moveTimePerFloorSeconds:(NSUInteger)moveTimePerFloorSeconds
            haltTimePerFloorSeconds:(NSUInteger)haltTimePerFloorSeconds;

- (void) pressFloorButtonForFloorIndex:(NSUInteger)floorIndex;
- (void) pressElevatorButtonForFloorIndex:(NSUInteger)floorIndex;

- (ESButtonState) elevatorButtonStateForFloorIndex:(NSUInteger)floorIndex;
- (ESButtonState) floorButtonStateForFloorIndex:(NSUInteger)floorIndex;

@property (nonatomic, weak) id<ESSimulatorProtocol> delegate;
@end

@protocol ESSimulatorProtocol
- (void)simulatorElevatorDidChangeFloor:(ESSimulator *)simulator;


@end
