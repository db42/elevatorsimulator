//
//  ESViewController.m
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/25/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import "ESViewController.h"
#import "ESSimulator.h"

static const NSUInteger FloorsCount = 8;
static const NSUInteger moveTimePerFloorSeconds = 1; //time to move to next floor
static const NSUInteger haltTimePerFloorSeconds = 2; //time to halt if floor button is pressed

@interface ESViewController () <ESSimulatorProtocol>
@property (nonatomic, strong) ESSimulator *simulator;
@property (strong, nonatomic) IBOutletCollection(UIButton)  NSArray *floorButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *elevatorPanelButtons;

@end

@implementation ESViewController
- (IBAction)floorButtonPressed:(UIButton *)sender {
  NSUInteger floorIndex = [self floorIndexForButton:sender];
  [self.simulator pressFloorButtonForFloorIndex:floorIndex];
  [self refreshView];
}

- (IBAction)liftButtonPressed:(UIButton *)sender {
  NSUInteger floorIndex = [self floorIndexForButton:sender];
  [self.simulator pressElevatorButtonForFloorIndex:floorIndex];
  [self refreshView];
}

- (ESSimulator *)simulator {
  if (_simulator == nil) {
    _simulator = [[ESSimulator alloc] initWithFloorsCount:FloorsCount
                                  moveTimePerFloorSeconds:moveTimePerFloorSeconds
                                  haltTimePerFloorSeconds:haltTimePerFloorSeconds];
    _simulator.delegate = self;
  }
  return _simulator;
}

- (NSUInteger)floorIndexForButton:(UIButton *)button {
  if ([button.titleLabel.text isEqualToString:@"G"]) {
    return 0;
  } else {
    return [button.titleLabel.text integerValue];
  }
}

- (void)refreshView {
  //update UI based on state of simulator
  [self refreshElevatorPanel];
  [self refreshFloors];
}

- (void)refreshElevatorPanel {
  for (UIButton *button in self.elevatorPanelButtons) {
    NSUInteger floorIndex = [self floorIndexForButton:button];
    ESButtonState state = [ self.simulator elevatorButtonStateForFloorIndex:floorIndex];
    [self updateButton:button withState:state];
  }
}

- (void)refreshFloors {
  for (UIButton *button in self.floorButtons) {
    NSUInteger floorIndex = [self floorIndexForButton:button];
    ESButtonState state = [ self.simulator floorButtonStateForFloorIndex:floorIndex];
    [self updateButton:button withState:state];
  }
}

- (void)updateButton:(UIButton *)button withState:(ESButtonState)state {
  switch (state) {
    case ESButtonStateNotPressed:
      [self setColorForButtonBox:button color:[self defaultTintColor]];
      button.enabled = YES;
      break;
    case ESButtonStatePressed:
      [self setColorForButtonBox:button color:[UIColor redColor]];
      button.enabled = YES;
      break;
    case ESButtonStateElevatorPresent:
      [self setColorForButtonBox:button color:[UIColor lightGrayColor]];
      button.enabled = NO;
      break;
  }
}

- (UIColor *)defaultTintColor {
  return [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  //SetUp floor buttons
  for(UIButton *button in self.floorButtons) {
    button.layer.cornerRadius = 19;
    [button.layer setBorderWidth:1.0];
    [self setColorForButtonBox:button color:[self defaultTintColor]];
  }
  
  //SetUp elevator panel buttons
  for(UIButton *button in self.elevatorPanelButtons) {
    button.layer.cornerRadius = 15;
    [button.layer setBorderWidth:1.0];
    [self setColorForButtonBox:button color:[self defaultTintColor]];
  }
  
  [self refreshView];
}

- (void)setColorForButtonBox:(UIButton *)button color:(UIColor *)color {
  button.titleLabel.textColor = color;
  [button.layer setBorderColor:[color CGColor]];
}

#pragma mark - ESSimulatorProtocol

- (void)simulatorElevatorDidChangeFloor:(ESSimulator *)simulator {
  [self refreshView];
}

@end
