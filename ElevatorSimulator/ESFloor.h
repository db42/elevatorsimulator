//
//  ESFloor.h
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/26/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESButtonState.h"

@interface ESFloor : NSObject

- (void)pressButton;
- (ESButtonState) buttonState;

@property (nonatomic) BOOL isElevatorPresent;

@end
