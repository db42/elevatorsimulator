//
//  main.m
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/25/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESAppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([ESAppDelegate class]));
  }
}
