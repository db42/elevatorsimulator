//
//  ESElevator.m
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/26/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import "ESElevator.h"
@interface ESElevator ()
@property (nonatomic) NSUInteger floorCount;
@property (nonatomic) NSMutableArray *buttonStates;
@end

@implementation ESElevator

- (instancetype)initWithFloorCount:(NSUInteger)floorCount startFloorIndex:(NSUInteger)startFloorIndex{
  self = [self init];
  if (self) {
    _floorCount = floorCount;
    _floorIndex = startFloorIndex;
    
    _buttonStates = [[NSMutableArray alloc] init];
    for (int index = 0; index < floorCount; index ++) {
      [_buttonStates addObject:[NSNumber numberWithInteger:ESButtonStateNotPressed]];
    }
    _buttonStates[startFloorIndex] = [NSNumber numberWithInteger:ESButtonStateElevatorPresent];
  }
  return self;
}

- (ESButtonState)buttonStateForFloor:(NSUInteger)floorIndex {
  NSNumber *number = self.buttonStates[floorIndex];
  return (ESButtonState)number.intValue;
}

- (void)pressButtonForFloor:(NSUInteger)floorIndex {
  self.buttonStates[floorIndex] = [NSNumber numberWithInt:ESButtonStatePressed];
}

- (void)setFloorIndex:(NSUInteger)floorIndex {
  if (_floorIndex != floorIndex) {
    self.buttonStates[_floorIndex] = [NSNumber numberWithInteger:ESButtonStateNotPressed];
    _floorIndex = floorIndex;
    self.buttonStates[_floorIndex] = [NSNumber numberWithInteger:ESButtonStateElevatorPresent];
  }
}

@end
