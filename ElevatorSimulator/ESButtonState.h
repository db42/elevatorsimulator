//
//  ESButtonState.h
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/27/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#ifndef ElevatorSimulator_ESButtonState_h
#define ElevatorSimulator_ESButtonState_h

typedef NS_ENUM(NSInteger, ESButtonState) {
  ESButtonStateElevatorPresent,
  ESButtonStatePressed,
  ESButtonStateNotPressed
};


#endif
