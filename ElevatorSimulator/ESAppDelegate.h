//
//  ESAppDelegate.h
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/25/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
