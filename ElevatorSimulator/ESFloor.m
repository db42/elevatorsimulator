//
//  ESFloor.m
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/26/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import "ESFloor.h"

@interface ESFloor ()
@property (nonatomic) BOOL isPressed;
@end

@implementation ESFloor
- (id)init {
  self = [super init];
  if (self) {
    _isPressed = NO;
    _isElevatorPresent = NO;
  }
  return self;
}

- (void)pressButton {
  if (!self.isElevatorPresent) {
    self.isPressed = YES;
  }
}

- (void)setIsElevatorPresent:(BOOL)isElevatorPresent {
  _isElevatorPresent = isElevatorPresent;
  self.isPressed = NO;
}

- (ESButtonState)buttonState {
  if (self.isElevatorPresent) {
    return ESButtonStateElevatorPresent;
  } else if (self.isPressed) {
    return ESButtonStatePressed;
  } else {
    return ESButtonStateNotPressed;
  }
}

@end
