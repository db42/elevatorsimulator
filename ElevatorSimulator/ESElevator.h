//
//  ESElevator.h
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/26/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESFloor.h"

@interface ESElevator : NSObject
@property (nonatomic) NSUInteger floorIndex;

- (instancetype)initWithFloorCount:(NSUInteger)floorCount startFloorIndex:(NSUInteger)startFloorIndex;
- (void)pressButtonForFloor:(NSUInteger)floorIndex;
- (ESButtonState)buttonStateForFloor:(NSUInteger)floorIndex;

@end
