//
//  ESSimulator.m
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/26/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import "ESSimulator.h"
#import "ESElevator.h"
#import "ESFloor.h"

static const NSUInteger StartFloorIndex = 0;

@interface ESSimulator ()
@property (nonatomic) NSMutableOrderedSet *elevatorDestinations;
@property (nonatomic) BOOL isElevatorMoving;
@property (nonatomic) NSMutableArray *floors;
@property (nonatomic) ESElevator *elevator;

@property (nonatomic) NSUInteger destinationFloor;
@property (nonatomic) NSUInteger moveTimePerFloorSeconds;
@property (nonatomic) NSUInteger haltTimePerFloorSeconds;
@end

@implementation ESSimulator

- (ESButtonState)elevatorButtonStateForFloorIndex:(NSUInteger)floorIndex {
  return [self.elevator buttonStateForFloor:floorIndex];
}

- (ESButtonState)floorButtonStateForFloorIndex:(NSUInteger)floorIndex {
  return [(ESFloor *)self.floors[floorIndex] buttonState];
}

- (instancetype)initWithFloorsCount:(NSUInteger)floorCount
            moveTimePerFloorSeconds:(NSUInteger)moveTimePerFloorSeconds
            haltTimePerFloorSeconds:(NSUInteger)haltTimePerFloorSeconds {
  self = [self init];
  if (self) {
    _moveTimePerFloorSeconds = moveTimePerFloorSeconds;
    _haltTimePerFloorSeconds = haltTimePerFloorSeconds;
    _elevator = [[ESElevator alloc] initWithFloorCount:floorCount startFloorIndex:StartFloorIndex];
    
    _floors = [[NSMutableArray alloc] init];
    for (int index = 0; index < floorCount; index ++) {
      [_floors addObject:[[ESFloor alloc] init]];
    }
    [(ESFloor *)_floors[StartFloorIndex] setIsElevatorPresent:YES];
    
    _isElevatorMoving = false;
    _elevatorDestinations = [[NSMutableOrderedSet alloc]  init];
  }
  return self;
}

- (void)pressElevatorButtonForFloorIndex:(NSUInteger)floorIndex {
  if ([self elevatorButtonStateForFloorIndex:floorIndex] != ESButtonStateElevatorPresent) {
    [self.elevator pressButtonForFloor:floorIndex];
    [self addToFloorDestinations:floorIndex];
  }
}

- (void)pressFloorButtonForFloorIndex:(NSUInteger)floorIndex {
  if ([self floorButtonStateForFloorIndex:floorIndex] != ESButtonStateElevatorPresent) {
    ESFloor *floor = (ESFloor *)self.floors[floorIndex];
    [floor pressButton];
    [self addToFloorDestinations:floorIndex];
  }
}

- (void)addToFloorDestinations:(NSUInteger)floorIndex {
  [self.elevatorDestinations addObject:[NSNumber numberWithInteger:floorIndex]];
  
  if (!self.isElevatorMoving) {
    [self startElevator];
  }
}

- (void)elevatorDidReachAtFloorIndex:(NSNumber *)floorIndex {
  //update floor
  [(ESFloor *)self.floors[self.elevator.floorIndex] setIsElevatorPresent:NO];
  [(ESFloor *)self.floors[floorIndex.integerValue] setIsElevatorPresent:YES];
  
  //update elevator
  self.elevator.floorIndex = floorIndex.integerValue;
  
  [self.delegate simulatorElevatorDidChangeFloor:self];
  
  //Halt if user has pressed button for this floor
  if ([self.elevatorDestinations containsObject:floorIndex]) {
    [self.elevatorDestinations removeObject:floorIndex];
    [self performSelector:@selector(moveElevatorToNextFloor)
               withObject:nil
               afterDelay:self.haltTimePerFloorSeconds];
  } else {
    [self moveElevatorToNextFloor];
  }
}

- (void)startElevator {
  if ([self.elevatorDestinations count] > 0) {
    NSNumber *floorToVisit = [self.elevatorDestinations objectAtIndex:0];
    self.destinationFloor = [floorToVisit integerValue];
    [self moveElevatorToNextFloor];
  }
}

- (void)moveElevatorToNextFloor {
  self.isElevatorMoving = YES;
  if (self.elevator.floorIndex != self.destinationFloor) {
    NSUInteger nextFloor = self.elevator.floorIndex;
    nextFloor = (self.destinationFloor > nextFloor) ? nextFloor + 1: nextFloor - 1;
    [self performSelector:@selector(elevatorDidReachAtFloorIndex:)
               withObject:[NSNumber numberWithInteger:nextFloor] afterDelay:self.moveTimePerFloorSeconds];
  } else {
    self.isElevatorMoving = NO;
    [self startElevator];
  }
}

@end
