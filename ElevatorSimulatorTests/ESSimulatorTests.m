//
//  ESSimulatorTests.m
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/27/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "ESSimulator.h"

@interface ESSimulatorTests : XCTestCase
@property (nonatomic) NSUInteger floorCount;
@property (nonatomic) ESSimulator *simulator;
@property (nonatomic) id mockSimulatorDelegate;

@end

@implementation ESSimulatorTests

- (void)setUp {
  [super setUp];
  
  self.floorCount = 5;
  self.simulator = [[ESSimulator alloc] initWithFloorsCount:self.floorCount
                                    moveTimePerFloorSeconds:0
                                    haltTimePerFloorSeconds:0];
  
  self.mockSimulatorDelegate = [OCMockObject mockForProtocol:@protocol(ESSimulatorProtocol)];
  self.simulator.delegate = self.mockSimulatorDelegate;
}

- (void)tearDown {
  [self.mockSimulatorDelegate verifyWithDelay:0.2];
  self.mockSimulatorDelegate = nil;
  self.simulator = nil;
  
  [super tearDown];
}

- (void)testItShouldReturnCorrectDefaultFloorsState {
  XCTAssertEqual([self.simulator floorButtonStateForFloorIndex:0], ESButtonStateElevatorPresent);
  for (int index = 1; index < self.floorCount; index++) {
    XCTAssertEqual([self.simulator floorButtonStateForFloorIndex:index], ESButtonStateNotPressed);
  }
}

- (void)testItShouldReturnCorrectDefaultElevatorPanelState {
  XCTAssertEqual([self.simulator elevatorButtonStateForFloorIndex:0], ESButtonStateElevatorPresent);
  for (int index = 1; index < self.floorCount; index++) {
    XCTAssertEqual([self.simulator elevatorButtonStateForFloorIndex:index], ESButtonStateNotPressed);
  }
}

- (void)testItShouldInformDelegateElevatorPositionChangeOnElevatorButtonPress {
  [self.simulator pressElevatorButtonForFloorIndex:1];
  id generatorDelegateExpectation = [self.mockSimulatorDelegate expect];
  [generatorDelegateExpectation simulatorElevatorDidChangeFloor:[OCMArg any]];
}

- (void)testItShouldInformDelegateElevatorPositionChangeOnFloorButtonPress {
  [self.simulator pressFloorButtonForFloorIndex:1];
  [[self.mockSimulatorDelegate expect] simulatorElevatorDidChangeFloor:[OCMArg any]];
}

- (void)testItShouldReturnCorrectFloorState {
  [self.simulator pressFloorButtonForFloorIndex:1];
  [[self.mockSimulatorDelegate expect] simulatorElevatorDidChangeFloor:[OCMArg any]];
  
  NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:0.1];
  while ([loopUntil timeIntervalSinceNow] > 0) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                             beforeDate:loopUntil];
  }
  for (int index = 0; index < self.floorCount; index++) {
    ESButtonState state = (index == 1) ? ESButtonStateElevatorPresent : ESButtonStateNotPressed;
    XCTAssertEqual([self.simulator floorButtonStateForFloorIndex:index], state);
  }
}

- (void)testItShouldReturnCorrectElevatorState {
  [self.simulator pressElevatorButtonForFloorIndex:1];
  [[self.mockSimulatorDelegate expect] simulatorElevatorDidChangeFloor:[OCMArg any]];
  
  NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:0.1];
  while ([loopUntil timeIntervalSinceNow] > 0) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                             beforeDate:loopUntil];
  }
  for (int index = 0; index < self.floorCount; index++) {
    ESButtonState state = (index == 1) ? ESButtonStateElevatorPresent : ESButtonStateNotPressed;
    XCTAssertEqual([self.simulator elevatorButtonStateForFloorIndex:index], state);
  }
}

@end
