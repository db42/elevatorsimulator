//
//  ESElevatorTests.m
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/27/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ESElevator.h"

@interface ESElevatorTests : XCTestCase

@end

@implementation ESElevatorTests

- (void)setUp {
  [super setUp];
  // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown {
  // Put teardown code here; it will be run once, after the last test case.
  [super tearDown];
}

- (void)testItShouldHaveCorrectDefaultButtonStates {
  NSUInteger floorCount = 5;
  NSUInteger startFloorIndex = arc4random() % floorCount;
  ESElevator *elevator = [[ESElevator alloc] initWithFloorCount:floorCount startFloorIndex:startFloorIndex];
  for (int index = 0; index < floorCount; index++) {
    ESButtonState state = (index == startFloorIndex) ? ESButtonStateElevatorPresent : ESButtonStateNotPressed;
    XCTAssertEqual([elevator buttonStateForFloor:index], state);
  }
}

- (void)testItShouldHaveCorrectButtonStates {
  NSUInteger floorCount = 3;
  ESElevator *elevator = [[ESElevator alloc] initWithFloorCount:floorCount startFloorIndex:0];
  
  elevator.floorIndex = 1;
  [elevator pressButtonForFloor:2];
  XCTAssertEqual([elevator buttonStateForFloor:0], ESButtonStateNotPressed);
  XCTAssertEqual([elevator buttonStateForFloor:1], ESButtonStateElevatorPresent);
  XCTAssertEqual([elevator buttonStateForFloor:2], ESButtonStatePressed);
}

@end
