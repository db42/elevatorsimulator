//
//  ESFloorTests.m
//  ElevatorSimulator
//
//  Created by Dushyant Bansal on 4/27/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ESFloor.h"

@interface ESFloorTests : XCTestCase

@end

@implementation ESFloorTests

- (void)setUp {
  [super setUp];
  // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown {
  // Put teardown code here; it will be run once, after the last test case.
  [super tearDown];
}

- (void)testItShouldHaveCorrectDefaultState {
  ESFloor *floor = [[ESFloor alloc] init];
  XCTAssertEqual(floor.buttonState, ESButtonStateNotPressed);
}

- (void)testItShouldHaveCorrectState {
  ESFloor *floor = [[ESFloor alloc] init];
  [floor pressButton];
  XCTAssertEqual(floor.buttonState, ESButtonStatePressed);
  
  floor.isElevatorPresent = YES;
  XCTAssertEqual(floor.buttonState, ESButtonStateElevatorPresent);
}



@end
